class LikesController < ApplicationController

    def show
        session[:micropost_id] = params[:id] || session[:micropost_id]
        @users = Micropost.find(session[:micropost_id]).likes.map(&:user)
    end

    def create

        @like = current_user.likes.find_by(micropost: params[:id])
        if @like.nil?
            @like = current_user.likes.build(micropost_id: params[:id])
            
            if !@like.save
                flash[:notice] = @like.errors.full_messages.to_sentence
            end
            # redirect_to request.referrer
        else
            @like.destroy
        end
        
        redirect_to request.referrer
        # respond_to do |format|
        #     format.html { redirect_to request.referrer }
        #     format.js
        # end
        
    end

    # def destroy
    #     @like = current_user.likes.find(params[:id])
    #     micropost = @like.Micropost
    #     @like.destroy
    #     redirect_to micropost 
    # end


    private

        # def like_params
        #     params.require(:like).permit(:micropost_id)
        # end

end
