class ApplicationMailer < ActionMailer::Base
  default from: 'jagadeesh@shipx.in'
  layout 'mailer'
end
